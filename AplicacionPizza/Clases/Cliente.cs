﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionPizza.Clases
{
    class Cliente
    {
        private string nombre;
        private string codigo;
        private string direccion;
        private string telefono;
        private string fax;
        private string celular;
        private string contacto;
        private string email;
        private string cedula_juridica;
        private string limite_credito;
        private string descuento;
        private string plazo_credito;
        private string estatus;
        private string exento;

        public Cliente(string codigo, string nombre, string direccion, string telefono, string fax, string celular, string contacto, string email, string cedula_juridica, string limite_credito, string descuento, string plazo_credito, string estatus, string exento)
        {
            this.nombre = nombre;
            this.codigo = codigo;
            this.direccion = direccion;
            this.telefono = telefono;
            this.fax = fax;
            this.celular = celular;
            this.contacto = contacto;
            this.email = email;
            this.cedula_juridica = cedula_juridica;
            this.limite_credito = limite_credito;
            this.descuento = descuento;
            this.plazo_credito = plazo_credito;
            this.estatus = estatus;
            this.exento = exento;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Fax
        {
            get
            {
                return fax;
            }

            set
            {
                fax = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Contacto
        {
            get
            {
                return contacto;
            }

            set
            {
                contacto = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Cedula_juridica
        {
            get
            {
                return cedula_juridica;
            }

            set
            {
                cedula_juridica = value;
            }
        }

        public string Limite_credito
        {
            get
            {
                return limite_credito;
            }

            set
            {
                limite_credito = value;
            }
        }

        public string Descuento
        {
            get
            {
                return descuento;
            }

            set
            {
                descuento = value;
            }
        }

        public string Plazo_credito
        {
            get
            {
                return plazo_credito;
            }

            set
            {
                plazo_credito = value;
            }
        }

        public string Estatus
        {
            get
            {
                return estatus;
            }

            set
            {
                estatus = value;
            }
        }

        public string Exento
        {
            get
            {
                return exento;
            }

            set
            {
                exento = value;
            }
        }
    }
}
