﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace AplicacionPizza.Clases
{
    class Conexion
    {
        public MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public Conexion()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "connectcsharptomysql";
            uid = "root";
            password = "";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Insert statement
        public void Insert()
        {
            string query = "INSERT INTO tableinfo (name, age) VALUES('Hola', '25')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Update statement
        public void Update()
        {
            string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

            //Open connection
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Delete statement
        public void Delete()
        {
            string query = "DELETE FROM tableinfo WHERE name='John Smith'";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        //Select statement
        public List<string>[] Select()
        {
            string query = "SELECT * FROM tableinfo";

            //Create a list to store the result
            List<string>[] list = new List<string>[3];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();

            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["name"] + "");
                    list[2].Add(dataReader["age"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        //Count statement
        public int Count()
        {
            string query = "SELECT Count(*) FROM tableinfo";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }
        }


        //Credenciales Consulta
        public bool CredencialesConsulta(string usuario, string contraseña) {

            string query = "SELECT usuario, contraseña FROM cuentas";
            string user, pass;

            if (this.OpenConnection()) {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read()) {
                    user = dataReader.GetString(0);
                    pass = dataReader.GetString(1);

                    if (user == usuario && pass == contraseña) {
                        CloseConnection();
                        return true;
                    }
                }
                MessageBox.Show("Credenciales Invalidas");
                CloseConnection();
                return false;
            }
            MessageBox.Show("Base de Datos no disponible");
            CloseConnection();
            return false;

        }

        //Insertar Productos
        public void InsertarProductos(Producto producto) {

            string query = "INSERT INTO inventario (codigo_articulo, descripcion, proveedor, existencia, exento, costo, precio) VALUES('"
                + producto.Codigo +"','" + producto.Nombre + "','" + producto.Proveedor + "','" + producto.Existencia + "','" + producto.Exento 
                + "','" + producto.Costo + "','" + producto.Precio + "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }


        //Insertar Clientes
        public void InsertarClientes(Cliente cliente)
        {

            string query = "INSERT INTO clientes (codigo_cliente, nombre_cliente, direccion, telefono, fax, celular, contacto, email,"
                +" cedula_juridica, limite_credito, descuento, plazo_credito, estatus) VALUES('" + cliente.Codigo + "','" + cliente.Nombre 
                + "','" + cliente.Direccion + "','" + cliente.Telefono + "','" + cliente.Fax + "','" + cliente.Celular + "','" + cliente.Contacto 
                + "','" + cliente.Email + "','" + cliente.Cedula_juridica + "','" + cliente.Limite_credito + "','" + cliente.Descuento + "','" 
                + cliente.Plazo_credito + "','" + cliente.Estatus +  "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }


        //Insertar Proveedores
        public void InsertarProveedores(Proveedor proveedor)
        {

            string query = "INSERT INTO proveedores (codigo_proveedor, nombre_proveedor, direccion, telefono, fax, celular, contacto, email, plazo_credito)"
                +"VALUES('"+ proveedor.Codigo + "','" + proveedor.Nombre + "','" + proveedor.Direccion + "','" + proveedor.Telefono + "','" + proveedor.Fax 
                + "','" + proveedor.Celular + "','" + proveedor.Contacto + "','" + proveedor.Email + "','" + proveedor.Plazo_credito + "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }



        //Backup
        public void Backup()
        {
        }

        //Restore
        public void Restore()
        {
        }
    }
}
