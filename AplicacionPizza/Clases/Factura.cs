﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionPizza.Clases
{
    class Factura
    {

        private long numero;
        private int moneda;
        private DateTime fecha;
        private string codigo_clt;
        private int exento;
        private double descclt;
        private string login;
        private string codigo_ven;
        private int tipo;
        private int estatus;
        private double subtotalex;
        private double subtotalgrav;
        private double descuento;
        private double impuesto;
        private double total;
        private int tipopago;
        private double saldo;
        private double cambio_dolar;
        private long plazo;
        private DateTime vencimiento;
        private string anombrede;
        private string observaciones;
        private string cliente;
        private string email;
        private string vendedor;


#region Sets y Gets de atributos
        public long Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public int Moneda
        {
            get
            {
                return moneda;
            }

            set
            {
                moneda = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public string Codigo_clt
        {
            get
            {
                return codigo_clt;
            }

            set
            {
                codigo_clt = value;
            }
        }

        public int Exento
        {
            get
            {
                return exento;
            }

            set
            {
                exento = value;
            }
        }

        public double Descclt
        {
            get
            {
                return descclt;
            }

            set
            {
                descclt = value;
            }
        }

        public string Login
        {
            get
            {
                return login;
            }

            set
            {
                login = value;
            }
        }

        public string Codigo_ven
        {
            get
            {
                return codigo_ven;
            }

            set
            {
                codigo_ven = value;
            }
        }

        public int Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        public int Estatus
        {
            get
            {
                return estatus;
            }

            set
            {
                estatus = value;
            }
        }

        public double Subtotalex
        {
            get
            {
                return subtotalex;
            }

            set
            {
                subtotalex = value;
            }
        }

        public double Subtotalgrav
        {
            get
            {
                return subtotalgrav;
            }

            set
            {
                subtotalgrav = value;
            }
        }

        public double Descuento
        {
            get
            {
                return descuento;
            }

            set
            {
                descuento = value;
            }
        }

        public double Impuesto
        {
            get
            {
                return impuesto;
            }

            set
            {
                impuesto = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Tipopago
        {
            get
            {
                return tipopago;
            }

            set
            {
                tipopago = value;
            }
        }

        public double Saldo
        {
            get
            {
                return saldo;
            }

            set
            {
                saldo = value;
            }
        }

        public double Cambio_dolar
        {
            get
            {
                return cambio_dolar;
            }

            set
            {
                cambio_dolar = value;
            }
        }

        public long Plazo
        {
            get
            {
                return plazo;
            }

            set
            {
                plazo = value;
            }
        }

        public DateTime Vencimiento
        {
            get
            {
                return vencimiento;
            }

            set
            {
                vencimiento = value;
            }
        }

        public string Anombrede
        {
            get
            {
                return anombrede;
            }

            set
            {
                anombrede = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public string Cliente
        {
            get
            {
                return cliente;
            }

            set
            {
                cliente = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Vendedor
        {
            get
            {
                return vendedor;
            }

            set
            {
                vendedor = value;
            }
        }

        #endregion

        public Factura()
        {
            Numero = 0;
            Moneda = 0;
            //Fecha = varconst.hoy;
            Codigo_clt = "";
            Exento = 0;
            Descclt = 0.00;
            Login = "";
            Codigo_ven = "";
            Tipo = 0;
            Estatus = 0;
            Subtotalex = 0.00;
            Subtotalgrav = 0.00;
            Descuento = 0.00;
            Impuesto = 0.00;
            Total = 0.00;
            Tipopago = 0;
            Saldo = 0.00;
            Cambio_dolar = 0.00;
            Plazo = 0;
            //Vencimiento = varconst.hoy;
            Anombrede = "";
            Observaciones = "";
            Cliente = "";
            Email = "";
            Vendedor = "";
        }

        public Factura(long numero, int moneda, DateTime fecha, string codigo_clt, int exento, double descclt, string login,
                       string codigo_ven, int tipo, int estatus, double subtotalex, double subtotalgrav,
                       double descuento, double impuesto, double total, int tipopago, double saldo,
                       double cambio_dolar, long plazo, DateTime vencimiento, string anombrede,
                       string observaciones, string cliente, string email, string vendedor)
        {
            this.Numero = numero;
            this.Moneda = moneda;
            this.Fecha = fecha;
            this.Codigo_clt = codigo_clt;
            this.Exento = exento;
            this.Descclt = descclt;
            this.Login = login;
            this.Codigo_ven = codigo_ven;
            this.Tipo = tipo;
            this.Estatus = estatus;
            this.Subtotalex = subtotalex;
            this.Subtotalgrav = subtotalgrav;
            this.Descuento = descuento;
            this.Impuesto = impuesto;
            this.Total = total;
            this.Tipopago = tipopago;
            this.Saldo = saldo;
            this.Cambio_dolar = cambio_dolar;
            this.Plazo = plazo;
            this.Vencimiento = vencimiento;
            this.Anombrede = anombrede;
            this.Observaciones = observaciones;
            this.Cliente = cliente;
            this.Email = email;
            this.Vendedor = vendedor;
        }

    }
}
