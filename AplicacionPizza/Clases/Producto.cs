﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionPizza.Clases
{
    class Producto
    {
        private string codigo;
        private string nombre;
        private string proveedor;
        private string existencia;
        private string exento;
        private double costo;
        private double precio;

        public Producto(string codigo, string nombre, string proveedor, string existencia, string exento, double costo, double precio)
        {
            this.Codigo = codigo;
            this.Nombre = nombre;
            this.Proveedor = proveedor;
            this.Existencia = existencia;
            this.Exento = exento;
            this.Costo = costo;
            this.Precio = precio;
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Proveedor
        {
            get
            {
                return proveedor;
            }

            set
            {
                proveedor = value;
            }
        }

        public string Existencia
        {
            get
            {
                return existencia;
            }

            set
            {
                existencia = value;
            }
        }

        public string Exento
        {
            get
            {
                return exento;
            }

            set
            {
                exento = value;
            }
        }

        public double Costo
        {
            get
            {
                return costo;
            }

            set
            {
                costo = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        
    }
}
