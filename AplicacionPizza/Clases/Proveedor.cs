﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionPizza.Clases
{
    class Proveedor
    {
        private string codigo;
        private string nombre;
        private string direccion;
        private string telefono;
        private string fax;
        private string celular;
        private string contacto;
        private string email;
        private string plazo_credito;

        public Proveedor(string codigo, string nombre, string direccion, string telefono, string fax, string celular, string contacto, string email, string plazo_credito)
        {
            this.codigo = codigo;
            this.nombre = nombre;
            this.direccion = direccion;
            this.telefono = telefono;
            this.fax = fax;
            this.celular = celular;
            this.contacto = contacto;
            this.email = email;
            this.plazo_credito = plazo_credito;
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Fax
        {
            get
            {
                return fax;
            }

            set
            {
                fax = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Contacto
        {
            get
            {
                return contacto;
            }

            set
            {
                contacto = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Plazo_credito
        {
            get
            {
                return plazo_credito;
            }

            set
            {
                plazo_credito = value;
            }
        }
    }
}
