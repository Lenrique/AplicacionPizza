﻿using AplicacionPizza.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplicacionPizza
{
    public partial class VentanaLogIn : Form
    {
        public VentanaLogIn()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Conexion conectar = new Conexion();
            if (conectar.CredencialesConsulta(textBoxUsuario.Text, textBoxContraseña.Text)) {
                this.Hide();
                VentanaMenu menu = new VentanaMenu();
                menu.Show();
            }
        }
    }
}
