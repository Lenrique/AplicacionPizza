﻿namespace AplicacionPizza
{
    partial class VentanaMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelIzq = new System.Windows.Forms.Panel();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonClientes = new System.Windows.Forms.Button();
            this.buttonFacturacion = new System.Windows.Forms.Button();
            this.buttonProveedores = new System.Windows.Forms.Button();
            this.buttonAjustes = new System.Windows.Forms.Button();
            this.buttonMesas = new System.Windows.Forms.Button();
            this.buttonProductos = new System.Windows.Forms.Button();
            this.buttonInicio = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelSuperiorInventario = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.botonBuscarProducto = new System.Windows.Forms.Button();
            this.textBoxProducto = new System.Windows.Forms.TextBox();
            this.panelInventario = new System.Windows.Forms.Panel();
            this.dataGridViewInventario = new System.Windows.Forms.DataGridView();
            this.codigo_articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exitencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelNuevoProducto = new System.Windows.Forms.Panel();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.textBoxPrecio = new System.Windows.Forms.TextBox();
            this.textBoxCosto = new System.Windows.Forms.TextBox();
            this.textBoxProveedor = new System.Windows.Forms.TextBox();
            this.textBoxDescripcion = new System.Windows.Forms.TextBox();
            this.textBoxCodigoProducto = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelMesas = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.panelFacturacion = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.dataGridViewFactura = new System.Windows.Forms.DataGridView();
            this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripción = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio_factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxImpuestoServicio = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxImpuesto = new System.Windows.Forms.TextBox();
            this.textBoxDescuento = new System.Windows.Forms.TextBox();
            this.textBoxSubTotalGravado = new System.Windows.Forms.TextBox();
            this.textBoxSubTotalExcento = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button26 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonMenos = new System.Windows.Forms.Button();
            this.buttonMas = new System.Windows.Forms.Button();
            this.textBoxFactura = new System.Windows.Forms.TextBox();
            this.botonAgregar = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.panelProveedores = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonNuevoProveedor = new System.Windows.Forms.Button();
            this.dataGridViewProveedores = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelNuevoCliente = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.checkBoxExento = new System.Windows.Forms.CheckBox();
            this.comboBoxEstatus = new System.Windows.Forms.ComboBox();
            this.textBoxPlazoCredito = new System.Windows.Forms.TextBox();
            this.textBoxDescuentoCliente = new System.Windows.Forms.TextBox();
            this.textBoxLimiteCredito = new System.Windows.Forms.TextBox();
            this.textBoxCedulaJuridica = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxContacto = new System.Windows.Forms.TextBox();
            this.textBoxFax = new System.Windows.Forms.TextBox();
            this.textBoxCelular = new System.Windows.Forms.TextBox();
            this.textBoxTelefono = new System.Windows.Forms.TextBox();
            this.textBoxDireccion = new System.Windows.Forms.TextBox();
            this.textBoxCodigoCliente = new System.Windows.Forms.TextBox();
            this.textBoxNombreCliente = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panelClientes = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonNuevoCliente = new System.Windows.Forms.Button();
            this.dataGridViewClientes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelNuevoProveedor = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.botonGuardarProveedor = new System.Windows.Forms.Button();
            this.textBoxPlazoCreditoProveedor = new System.Windows.Forms.TextBox();
            this.textBoxEmailProveedor = new System.Windows.Forms.TextBox();
            this.textBoxContactoProveedor = new System.Windows.Forms.TextBox();
            this.textBoxFaxProveedor = new System.Windows.Forms.TextBox();
            this.textBoxCelularProveedor = new System.Windows.Forms.TextBox();
            this.textBoxTelefonoProveedor = new System.Windows.Forms.TextBox();
            this.textBoxDireccionProveedor = new System.Windows.Forms.TextBox();
            this.textBoxCodigoProveedor = new System.Windows.Forms.TextBox();
            this.textBoxNombreProveedor = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxExistencia = new System.Windows.Forms.TextBox();
            this.checkBoxExcento = new System.Windows.Forms.CheckBox();
            this.panelIzq.SuspendLayout();
            this.panelSuperiorInventario.SuspendLayout();
            this.panelInventario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInventario)).BeginInit();
            this.panelNuevoProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMesas.SuspendLayout();
            this.panelFacturacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFactura)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelProveedores.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProveedores)).BeginInit();
            this.panelNuevoCliente.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelClientes.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientes)).BeginInit();
            this.panelNuevoProveedor.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelIzq
            // 
            this.panelIzq.BackColor = System.Drawing.Color.DimGray;
            this.panelIzq.Controls.Add(this.buttonLogout);
            this.panelIzq.Controls.Add(this.buttonClientes);
            this.panelIzq.Controls.Add(this.buttonFacturacion);
            this.panelIzq.Controls.Add(this.buttonProveedores);
            this.panelIzq.Controls.Add(this.buttonAjustes);
            this.panelIzq.Controls.Add(this.buttonMesas);
            this.panelIzq.Controls.Add(this.buttonProductos);
            this.panelIzq.Controls.Add(this.buttonInicio);
            this.panelIzq.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelIzq.Location = new System.Drawing.Point(0, 68);
            this.panelIzq.Name = "panelIzq";
            this.panelIzq.Size = new System.Drawing.Size(200, 641);
            this.panelIzq.TabIndex = 4;
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.DimGray;
            this.buttonLogout.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLogout.FlatAppearance.BorderSize = 0;
            this.buttonLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogout.ForeColor = System.Drawing.Color.White;
            this.buttonLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLogout.Location = new System.Drawing.Point(0, 434);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(200, 62);
            this.buttonLogout.TabIndex = 7;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = false;
            // 
            // buttonClientes
            // 
            this.buttonClientes.BackColor = System.Drawing.Color.DimGray;
            this.buttonClientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonClientes.FlatAppearance.BorderSize = 0;
            this.buttonClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientes.ForeColor = System.Drawing.Color.White;
            this.buttonClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClientes.Location = new System.Drawing.Point(0, 372);
            this.buttonClientes.Name = "buttonClientes";
            this.buttonClientes.Size = new System.Drawing.Size(200, 62);
            this.buttonClientes.TabIndex = 6;
            this.buttonClientes.Text = "Clientes";
            this.buttonClientes.UseVisualStyleBackColor = false;
            this.buttonClientes.Click += new System.EventHandler(this.button9_Click);
            // 
            // buttonFacturacion
            // 
            this.buttonFacturacion.BackColor = System.Drawing.Color.DimGray;
            this.buttonFacturacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonFacturacion.FlatAppearance.BorderSize = 0;
            this.buttonFacturacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFacturacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFacturacion.ForeColor = System.Drawing.Color.White;
            this.buttonFacturacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFacturacion.Location = new System.Drawing.Point(0, 310);
            this.buttonFacturacion.Name = "buttonFacturacion";
            this.buttonFacturacion.Size = new System.Drawing.Size(200, 62);
            this.buttonFacturacion.TabIndex = 5;
            this.buttonFacturacion.Text = "Facturación";
            this.buttonFacturacion.UseVisualStyleBackColor = false;
            this.buttonFacturacion.Click += new System.EventHandler(this.button8_Click);
            // 
            // buttonProveedores
            // 
            this.buttonProveedores.BackColor = System.Drawing.Color.DimGray;
            this.buttonProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonProveedores.FlatAppearance.BorderSize = 0;
            this.buttonProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProveedores.ForeColor = System.Drawing.Color.White;
            this.buttonProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonProveedores.Location = new System.Drawing.Point(0, 248);
            this.buttonProveedores.Name = "buttonProveedores";
            this.buttonProveedores.Size = new System.Drawing.Size(200, 62);
            this.buttonProveedores.TabIndex = 4;
            this.buttonProveedores.Text = "  Proveedores";
            this.buttonProveedores.UseVisualStyleBackColor = false;
            this.buttonProveedores.Click += new System.EventHandler(this.button7_Click);
            // 
            // buttonAjustes
            // 
            this.buttonAjustes.BackColor = System.Drawing.Color.DimGray;
            this.buttonAjustes.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAjustes.FlatAppearance.BorderSize = 0;
            this.buttonAjustes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAjustes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjustes.ForeColor = System.Drawing.Color.White;
            this.buttonAjustes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAjustes.Location = new System.Drawing.Point(0, 186);
            this.buttonAjustes.Name = "buttonAjustes";
            this.buttonAjustes.Size = new System.Drawing.Size(200, 62);
            this.buttonAjustes.TabIndex = 3;
            this.buttonAjustes.Text = "Ajustes";
            this.buttonAjustes.UseVisualStyleBackColor = false;
            // 
            // buttonMesas
            // 
            this.buttonMesas.BackColor = System.Drawing.Color.DimGray;
            this.buttonMesas.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonMesas.FlatAppearance.BorderSize = 0;
            this.buttonMesas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMesas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMesas.ForeColor = System.Drawing.Color.White;
            this.buttonMesas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonMesas.Location = new System.Drawing.Point(0, 124);
            this.buttonMesas.Name = "buttonMesas";
            this.buttonMesas.Size = new System.Drawing.Size(200, 62);
            this.buttonMesas.TabIndex = 2;
            this.buttonMesas.Text = "Mesas";
            this.buttonMesas.UseVisualStyleBackColor = false;
            this.buttonMesas.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonProductos
            // 
            this.buttonProductos.BackColor = System.Drawing.Color.DimGray;
            this.buttonProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonProductos.FlatAppearance.BorderSize = 0;
            this.buttonProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonProductos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProductos.ForeColor = System.Drawing.Color.White;
            this.buttonProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonProductos.Location = new System.Drawing.Point(0, 62);
            this.buttonProductos.Name = "buttonProductos";
            this.buttonProductos.Size = new System.Drawing.Size(200, 62);
            this.buttonProductos.TabIndex = 1;
            this.buttonProductos.Text = "Productos";
            this.buttonProductos.UseVisualStyleBackColor = false;
            this.buttonProductos.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonInicio
            // 
            this.buttonInicio.BackColor = System.Drawing.Color.DimGray;
            this.buttonInicio.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonInicio.FlatAppearance.BorderSize = 0;
            this.buttonInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInicio.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInicio.ForeColor = System.Drawing.Color.White;
            this.buttonInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInicio.Location = new System.Drawing.Point(0, 0);
            this.buttonInicio.Name = "buttonInicio";
            this.buttonInicio.Size = new System.Drawing.Size(200, 62);
            this.buttonInicio.TabIndex = 0;
            this.buttonInicio.Text = "Inicio";
            this.buttonInicio.UseVisualStyleBackColor = false;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1307, 68);
            this.panelTop.TabIndex = 6;
            // 
            // panelSuperiorInventario
            // 
            this.panelSuperiorInventario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSuperiorInventario.BackColor = System.Drawing.Color.White;
            this.panelSuperiorInventario.Controls.Add(this.button3);
            this.panelSuperiorInventario.Controls.Add(this.botonBuscarProducto);
            this.panelSuperiorInventario.Controls.Add(this.textBoxProducto);
            this.panelSuperiorInventario.Location = new System.Drawing.Point(0, 0);
            this.panelSuperiorInventario.Name = "panelSuperiorInventario";
            this.panelSuperiorInventario.Size = new System.Drawing.Size(1107, 62);
            this.panelSuperiorInventario.TabIndex = 7;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Highlight;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(959, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 26);
            this.button3.TabIndex = 3;
            this.button3.Text = "Nuevo Producto";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // botonBuscarProducto
            // 
            this.botonBuscarProducto.Location = new System.Drawing.Point(356, 23);
            this.botonBuscarProducto.Name = "botonBuscarProducto";
            this.botonBuscarProducto.Size = new System.Drawing.Size(75, 23);
            this.botonBuscarProducto.TabIndex = 2;
            this.botonBuscarProducto.Text = "Buscar";
            this.botonBuscarProducto.UseVisualStyleBackColor = true;
            this.botonBuscarProducto.Click += new System.EventHandler(this.botonBuscarProducto_Click);
            // 
            // textBoxProducto
            // 
            this.textBoxProducto.AutoCompleteCustomSource.AddRange(new string[] {
            "Costa Rica",
            "NIGERIA"});
            this.textBoxProducto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxProducto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxProducto.Location = new System.Drawing.Point(42, 25);
            this.textBoxProducto.Name = "textBoxProducto";
            this.textBoxProducto.Size = new System.Drawing.Size(305, 20);
            this.textBoxProducto.TabIndex = 0;
            // 
            // panelInventario
            // 
            this.panelInventario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelInventario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelInventario.Controls.Add(this.panelSuperiorInventario);
            this.panelInventario.Controls.Add(this.dataGridViewInventario);
            this.panelInventario.Location = new System.Drawing.Point(200, 68);
            this.panelInventario.Name = "panelInventario";
            this.panelInventario.Size = new System.Drawing.Size(1107, 641);
            this.panelInventario.TabIndex = 8;
            // 
            // dataGridViewInventario
            // 
            this.dataGridViewInventario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewInventario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewInventario.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewInventario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewInventario.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewInventario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewInventario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInventario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo_articulo,
            this.descripcion,
            this.proveedor,
            this.exitencia,
            this.exento,
            this.costo,
            this.precio});
            this.dataGridViewInventario.EnableHeadersVisualStyles = false;
            this.dataGridViewInventario.GridColor = System.Drawing.Color.White;
            this.dataGridViewInventario.Location = new System.Drawing.Point(21, 123);
            this.dataGridViewInventario.Name = "dataGridViewInventario";
            this.dataGridViewInventario.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewInventario.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewInventario.RowHeadersVisible = false;
            this.dataGridViewInventario.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewInventario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewInventario.Size = new System.Drawing.Size(1061, 495);
            this.dataGridViewInventario.TabIndex = 0;
            this.dataGridViewInventario.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewInventario_CellDoubleClick);
            // 
            // codigo_articulo
            // 
            this.codigo_articulo.HeaderText = "Codigo Articulo";
            this.codigo_articulo.Name = "codigo_articulo";
            this.codigo_articulo.ReadOnly = true;
            // 
            // descripcion
            // 
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            // 
            // proveedor
            // 
            this.proveedor.HeaderText = "Proveedor";
            this.proveedor.Name = "proveedor";
            this.proveedor.ReadOnly = true;
            // 
            // exitencia
            // 
            this.exitencia.HeaderText = "Existencia";
            this.exitencia.Name = "exitencia";
            this.exitencia.ReadOnly = true;
            // 
            // exento
            // 
            this.exento.HeaderText = "Exento";
            this.exento.Name = "exento";
            this.exento.ReadOnly = true;
            // 
            // costo
            // 
            this.costo.HeaderText = "Costo";
            this.costo.Name = "costo";
            this.costo.ReadOnly = true;
            // 
            // precio
            // 
            this.precio.HeaderText = "Precio";
            this.precio.Name = "precio";
            this.precio.ReadOnly = true;
            // 
            // panelNuevoProducto
            // 
            this.panelNuevoProducto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelNuevoProducto.Controls.Add(this.checkBoxExcento);
            this.panelNuevoProducto.Controls.Add(this.textBoxExistencia);
            this.panelNuevoProducto.Controls.Add(this.btnGuardar);
            this.panelNuevoProducto.Controls.Add(this.textBoxPrecio);
            this.panelNuevoProducto.Controls.Add(this.textBoxCosto);
            this.panelNuevoProducto.Controls.Add(this.textBoxProveedor);
            this.panelNuevoProducto.Controls.Add(this.textBoxDescripcion);
            this.panelNuevoProducto.Controls.Add(this.textBoxCodigoProducto);
            this.panelNuevoProducto.Controls.Add(this.label7);
            this.panelNuevoProducto.Controls.Add(this.label6);
            this.panelNuevoProducto.Controls.Add(this.label5);
            this.panelNuevoProducto.Controls.Add(this.label4);
            this.panelNuevoProducto.Controls.Add(this.label3);
            this.panelNuevoProducto.Controls.Add(this.label2);
            this.panelNuevoProducto.Controls.Add(this.label1);
            this.panelNuevoProducto.Controls.Add(this.pictureBox1);
            this.panelNuevoProducto.ForeColor = System.Drawing.Color.Black;
            this.panelNuevoProducto.Location = new System.Drawing.Point(200, 68);
            this.panelNuevoProducto.Name = "panelNuevoProducto";
            this.panelNuevoProducto.Size = new System.Drawing.Size(1107, 641);
            this.panelNuevoProducto.TabIndex = 9;
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnGuardar.Location = new System.Drawing.Point(985, 538);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(80, 28);
            this.btnGuardar.TabIndex = 16;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // textBoxPrecio
            // 
            this.textBoxPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrecio.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxPrecio.Location = new System.Drawing.Point(380, 401);
            this.textBoxPrecio.Name = "textBoxPrecio";
            this.textBoxPrecio.Size = new System.Drawing.Size(500, 25);
            this.textBoxPrecio.TabIndex = 13;
            // 
            // textBoxCosto
            // 
            this.textBoxCosto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCosto.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxCosto.Location = new System.Drawing.Point(380, 356);
            this.textBoxCosto.Name = "textBoxCosto";
            this.textBoxCosto.Size = new System.Drawing.Size(500, 25);
            this.textBoxCosto.TabIndex = 12;
            // 
            // textBoxProveedor
            // 
            this.textBoxProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxProveedor.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxProveedor.Location = new System.Drawing.Point(380, 256);
            this.textBoxProveedor.Name = "textBoxProveedor";
            this.textBoxProveedor.Size = new System.Drawing.Size(500, 25);
            this.textBoxProveedor.TabIndex = 11;
            // 
            // textBoxDescripcion
            // 
            this.textBoxDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDescripcion.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxDescripcion.Location = new System.Drawing.Point(380, 206);
            this.textBoxDescripcion.Name = "textBoxDescripcion";
            this.textBoxDescripcion.Size = new System.Drawing.Size(500, 25);
            this.textBoxDescripcion.TabIndex = 10;
            // 
            // textBoxCodigoProducto
            // 
            this.textBoxCodigoProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCodigoProducto.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxCodigoProducto.Location = new System.Drawing.Point(380, 156);
            this.textBoxCodigoProducto.Name = "textBoxCodigoProducto";
            this.textBoxCodigoProducto.Size = new System.Drawing.Size(500, 25);
            this.textBoxCodigoProducto.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label7.Location = new System.Drawing.Point(295, 406);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Precio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label6.Location = new System.Drawing.Point(296, 356);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Costo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label5.Location = new System.Drawing.Point(266, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Existencia";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(266, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Proveedor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(256, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Descripción";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(196, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Código del Producto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(101, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Información del Producto";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AplicacionPizza.Properties.Resources.ic_mode_edit_black_48dp_2x;
            this.pictureBox1.Location = new System.Drawing.Point(64, 67);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelMesas
            // 
            this.panelMesas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelMesas.Controls.Add(this.button11);
            this.panelMesas.Location = new System.Drawing.Point(200, 68);
            this.panelMesas.Name = "panelMesas";
            this.panelMesas.Size = new System.Drawing.Size(1107, 641);
            this.panelMesas.TabIndex = 11;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(949, 459);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 0;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // panelFacturacion
            // 
            this.panelFacturacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFacturacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelFacturacion.Controls.Add(this.button12);
            this.panelFacturacion.Controls.Add(this.dataGridViewFactura);
            this.panelFacturacion.Controls.Add(this.panel3);
            this.panelFacturacion.Controls.Add(this.panel2);
            this.panelFacturacion.Controls.Add(this.panel1);
            this.panelFacturacion.Location = new System.Drawing.Point(200, 68);
            this.panelFacturacion.Name = "panelFacturacion";
            this.panelFacturacion.Size = new System.Drawing.Size(1107, 641);
            this.panelFacturacion.TabIndex = 12;
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.BackColor = System.Drawing.Color.LimeGreen;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(726, 579);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(369, 34);
            this.button12.TabIndex = 4;
            this.button12.Text = "Completar Venta";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // dataGridViewFactura
            // 
            this.dataGridViewFactura.AllowUserToAddRows = false;
            this.dataGridViewFactura.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFactura.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFactura.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewFactura.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFactura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo,
            this.descripción,
            this.precio_factura,
            this.descuento,
            this.cantidad,
            this.total});
            this.dataGridViewFactura.GridColor = System.Drawing.Color.White;
            this.dataGridViewFactura.Location = new System.Drawing.Point(21, 114);
            this.dataGridViewFactura.Name = "dataGridViewFactura";
            this.dataGridViewFactura.RowHeadersVisible = false;
            this.dataGridViewFactura.Size = new System.Drawing.Size(679, 499);
            this.dataGridViewFactura.TabIndex = 3;
            this.dataGridViewFactura.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFactura_CellValueChanged);
            // 
            // codigo
            // 
            this.codigo.HeaderText = "Código";
            this.codigo.Name = "codigo";
            this.codigo.ReadOnly = true;
            // 
            // descripción
            // 
            this.descripción.HeaderText = "Descripción";
            this.descripción.Name = "descripción";
            this.descripción.ReadOnly = true;
            // 
            // precio_factura
            // 
            this.precio_factura.HeaderText = "Precio";
            this.precio_factura.Name = "precio_factura";
            this.precio_factura.ReadOnly = true;
            // 
            // descuento
            // 
            this.descuento.HeaderText = "Descuento";
            this.descuento.Name = "descuento";
            // 
            // cantidad
            // 
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            // 
            // total
            // 
            this.total.HeaderText = "Total";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.textBoxImpuestoServicio);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.textBoxImpuesto);
            this.panel3.Controls.Add(this.textBoxDescuento);
            this.panel3.Controls.Add(this.textBoxSubTotalGravado);
            this.panel3.Controls.Add(this.textBoxSubTotalExcento);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.textBoxTotal);
            this.panel3.Controls.Add(this.button22);
            this.panel3.Controls.Add(this.button21);
            this.panel3.Controls.Add(this.button20);
            this.panel3.Location = new System.Drawing.Point(726, 186);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(369, 376);
            this.panel3.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 325);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 20);
            this.label14.TabIndex = 14;
            this.label14.Text = "Total";
            // 
            // textBoxImpuestoServicio
            // 
            this.textBoxImpuestoServicio.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxImpuestoServicio.Location = new System.Drawing.Point(217, 160);
            this.textBoxImpuestoServicio.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxImpuestoServicio.Name = "textBoxImpuestoServicio";
            this.textBoxImpuestoServicio.ReadOnly = true;
            this.textBoxImpuestoServicio.Size = new System.Drawing.Size(104, 20);
            this.textBoxImpuestoServicio.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(138, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "SubTotal Gravado";
            // 
            // textBoxImpuesto
            // 
            this.textBoxImpuesto.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxImpuesto.Location = new System.Drawing.Point(217, 121);
            this.textBoxImpuesto.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxImpuesto.Name = "textBoxImpuesto";
            this.textBoxImpuesto.ReadOnly = true;
            this.textBoxImpuesto.Size = new System.Drawing.Size(104, 20);
            this.textBoxImpuesto.TabIndex = 11;
            // 
            // textBoxDescuento
            // 
            this.textBoxDescuento.Location = new System.Drawing.Point(217, 89);
            this.textBoxDescuento.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDescuento.Name = "textBoxDescuento";
            this.textBoxDescuento.Size = new System.Drawing.Size(104, 20);
            this.textBoxDescuento.TabIndex = 10;
            // 
            // textBoxSubTotalGravado
            // 
            this.textBoxSubTotalGravado.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxSubTotalGravado.Location = new System.Drawing.Point(216, 61);
            this.textBoxSubTotalGravado.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSubTotalGravado.Name = "textBoxSubTotalGravado";
            this.textBoxSubTotalGravado.ReadOnly = true;
            this.textBoxSubTotalGravado.Size = new System.Drawing.Size(104, 20);
            this.textBoxSubTotalGravado.TabIndex = 9;
            // 
            // textBoxSubTotalExcento
            // 
            this.textBoxSubTotalExcento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSubTotalExcento.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxSubTotalExcento.Location = new System.Drawing.Point(217, 29);
            this.textBoxSubTotalExcento.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSubTotalExcento.Name = "textBoxSubTotalExcento";
            this.textBoxSubTotalExcento.ReadOnly = true;
            this.textBoxSubTotalExcento.Size = new System.Drawing.Size(104, 20);
            this.textBoxSubTotalExcento.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 20);
            this.label12.TabIndex = 7;
            this.label12.Text = "Impuesto Servicio";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 123);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.Size = new System.Drawing.Size(76, 20);
            this.label11.TabIndex = 6;
            this.label11.Text = "Impuesto";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 20);
            this.label10.TabIndex = 5;
            this.label10.Text = "SubTotal";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 20);
            this.label9.TabIndex = 4;
            this.label9.Text = "Descuento";
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTotal.Location = new System.Drawing.Point(82, 326);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.ReadOnly = true;
            this.textBoxTotal.Size = new System.Drawing.Size(252, 20);
            this.textBoxTotal.TabIndex = 3;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(259, 270);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 2;
            this.button22.Text = "Cheque";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(145, 270);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 1;
            this.button21.Text = "Tarjeta";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(27, 270);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 0;
            this.button20.Text = "Efectivo";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.button26);
            this.panel2.Controls.Add(this.textBox5);
            this.panel2.Controls.Add(this.textBox4);
            this.panel2.Controls.Add(this.button19);
            this.panel2.Controls.Add(this.button18);
            this.panel2.Controls.Add(this.button17);
            this.panel2.Controls.Add(this.button16);
            this.panel2.Controls.Add(this.button15);
            this.panel2.Location = new System.Drawing.Point(726, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(369, 145);
            this.panel2.TabIndex = 1;
            // 
            // button26
            // 
            this.button26.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_search_black_24dp;
            this.button26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button26.Location = new System.Drawing.Point(298, 64);
            this.button26.Margin = new System.Windows.Forms.Padding(2);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(35, 35);
            this.button26.TabIndex = 7;
            this.button26.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(92, 66);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(205, 20);
            this.textBox5.TabIndex = 6;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(49, 14);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(49, 20);
            this.textBox4.TabIndex = 5;
            // 
            // button19
            // 
            this.button19.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_person_add_black_24dp;
            this.button19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button19.Location = new System.Drawing.Point(32, 65);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(35, 35);
            this.button19.TabIndex = 4;
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(13, 13);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(30, 23);
            this.button18.TabIndex = 3;
            this.button18.Text = "#";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(308, 13);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(42, 23);
            this.button17.TabIndex = 2;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(215, 11);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(46, 23);
            this.button16.TabIndex = 1;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_pause_circle_filled_black_24dp_2x;
            this.button15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button15.Location = new System.Drawing.Point(157, 13);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(32, 23);
            this.button15.TabIndex = 0;
            this.button15.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonEliminar);
            this.panel1.Controls.Add(this.buttonMenos);
            this.panel1.Controls.Add(this.buttonMas);
            this.panel1.Controls.Add(this.textBoxFactura);
            this.panel1.Controls.Add(this.botonAgregar);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Location = new System.Drawing.Point(21, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(679, 69);
            this.panel1.TabIndex = 0;
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_delete_black_48dp_2x;
            this.buttonEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonEliminar.Location = new System.Drawing.Point(614, 13);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(35, 35);
            this.buttonEliminar.TabIndex = 5;
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.button25_Click);
            // 
            // buttonMenos
            // 
            this.buttonMenos.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_remove_circle_black_48dp_2x;
            this.buttonMenos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMenos.Location = new System.Drawing.Point(572, 13);
            this.buttonMenos.Name = "buttonMenos";
            this.buttonMenos.Size = new System.Drawing.Size(35, 35);
            this.buttonMenos.TabIndex = 4;
            this.buttonMenos.UseVisualStyleBackColor = true;
            this.buttonMenos.Click += new System.EventHandler(this.button24_Click);
            // 
            // buttonMas
            // 
            this.buttonMas.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_add_circle_black_48dp_2x;
            this.buttonMas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMas.Location = new System.Drawing.Point(529, 13);
            this.buttonMas.Name = "buttonMas";
            this.buttonMas.Size = new System.Drawing.Size(35, 35);
            this.buttonMas.TabIndex = 3;
            this.buttonMas.UseVisualStyleBackColor = true;
            this.buttonMas.Click += new System.EventHandler(this.button23_Click);
            // 
            // textBoxFactura
            // 
            this.textBoxFactura.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBoxFactura.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFactura.Location = new System.Drawing.Point(65, 19);
            this.textBoxFactura.Name = "textBoxFactura";
            this.textBoxFactura.Size = new System.Drawing.Size(320, 25);
            this.textBoxFactura.TabIndex = 2;
            this.textBoxFactura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxFactura_KeyPress);
            // 
            // botonAgregar
            // 
            this.botonAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.botonAgregar.Location = new System.Drawing.Point(384, 18);
            this.botonAgregar.Name = "botonAgregar";
            this.botonAgregar.Size = new System.Drawing.Size(75, 27);
            this.botonAgregar.TabIndex = 1;
            this.botonAgregar.Text = "Agregar";
            this.botonAgregar.UseVisualStyleBackColor = true;
            this.botonAgregar.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Gainsboro;
            this.button13.BackgroundImage = global::AplicacionPizza.Properties.Resources.ic_mode_edit_black_48dp_2x;
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.ForeColor = System.Drawing.Color.Transparent;
            this.button13.Location = new System.Drawing.Point(34, 18);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(32, 27);
            this.button13.TabIndex = 0;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // panelProveedores
            // 
            this.panelProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelProveedores.Controls.Add(this.panel6);
            this.panelProveedores.Controls.Add(this.dataGridViewProveedores);
            this.panelProveedores.Location = new System.Drawing.Point(200, 68);
            this.panelProveedores.Name = "panelProveedores";
            this.panelProveedores.Size = new System.Drawing.Size(1107, 641);
            this.panelProveedores.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.buttonNuevoProveedor);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1107, 62);
            this.panel6.TabIndex = 3;
            // 
            // buttonNuevoProveedor
            // 
            this.buttonNuevoProveedor.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonNuevoProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNuevoProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNuevoProveedor.ForeColor = System.Drawing.Color.White;
            this.buttonNuevoProveedor.Location = new System.Drawing.Point(949, 19);
            this.buttonNuevoProveedor.Name = "buttonNuevoProveedor";
            this.buttonNuevoProveedor.Size = new System.Drawing.Size(133, 26);
            this.buttonNuevoProveedor.TabIndex = 0;
            this.buttonNuevoProveedor.Text = "Nuevo Proveedor";
            this.buttonNuevoProveedor.UseVisualStyleBackColor = false;
            this.buttonNuevoProveedor.Click += new System.EventHandler(this.buttonNuevoProveedor_Click);
            // 
            // dataGridViewProveedores
            // 
            this.dataGridViewProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewProveedores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewProveedores.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewProveedores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewProveedores.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProveedores.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProveedores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn8,
            this.direccion,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dataGridViewProveedores.EnableHeadersVisualStyles = false;
            this.dataGridViewProveedores.GridColor = System.Drawing.Color.White;
            this.dataGridViewProveedores.Location = new System.Drawing.Point(21, 123);
            this.dataGridViewProveedores.Name = "dataGridViewProveedores";
            this.dataGridViewProveedores.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProveedores.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewProveedores.RowHeadersVisible = false;
            this.dataGridViewProveedores.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewProveedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProveedores.Size = new System.Drawing.Size(1061, 495);
            this.dataGridViewProveedores.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Codigo Proveedor";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // direccion
            // 
            this.direccion.HeaderText = "Direccion";
            this.direccion.Name = "direccion";
            this.direccion.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Teléfono";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Celular";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Contacto";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Email";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Plazo";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // panelNuevoCliente
            // 
            this.panelNuevoCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelNuevoCliente.Controls.Add(this.panel4);
            this.panelNuevoCliente.Location = new System.Drawing.Point(200, 68);
            this.panelNuevoCliente.Name = "panelNuevoCliente";
            this.panelNuevoCliente.Size = new System.Drawing.Size(1107, 641);
            this.panelNuevoCliente.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.buttonGuardar);
            this.panel4.Controls.Add(this.checkBoxExento);
            this.panel4.Controls.Add(this.comboBoxEstatus);
            this.panel4.Controls.Add(this.textBoxPlazoCredito);
            this.panel4.Controls.Add(this.textBoxDescuentoCliente);
            this.panel4.Controls.Add(this.textBoxLimiteCredito);
            this.panel4.Controls.Add(this.textBoxCedulaJuridica);
            this.panel4.Controls.Add(this.textBoxEmail);
            this.panel4.Controls.Add(this.textBoxContacto);
            this.panel4.Controls.Add(this.textBoxFax);
            this.panel4.Controls.Add(this.textBoxCelular);
            this.panel4.Controls.Add(this.textBoxTelefono);
            this.panel4.Controls.Add(this.textBoxDireccion);
            this.panel4.Controls.Add(this.textBoxCodigoCliente);
            this.panel4.Controls.Add(this.textBoxNombreCliente);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Location = new System.Drawing.Point(42, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1023, 583);
            this.panel4.TabIndex = 0;
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(866, 512);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(75, 23);
            this.buttonGuardar.TabIndex = 28;
            this.buttonGuardar.Text = "Guardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // checkBoxExento
            // 
            this.checkBoxExento.AutoSize = true;
            this.checkBoxExento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxExento.Location = new System.Drawing.Point(726, 365);
            this.checkBoxExento.Name = "checkBoxExento";
            this.checkBoxExento.Size = new System.Drawing.Size(86, 24);
            this.checkBoxExento.TabIndex = 27;
            this.checkBoxExento.Text = "Excento";
            this.checkBoxExento.UseVisualStyleBackColor = true;
            // 
            // comboBoxEstatus
            // 
            this.comboBoxEstatus.DisplayMember = "0";
            this.comboBoxEstatus.FormattingEnabled = true;
            this.comboBoxEstatus.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.comboBoxEstatus.Location = new System.Drawing.Point(726, 313);
            this.comboBoxEstatus.Name = "comboBoxEstatus";
            this.comboBoxEstatus.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEstatus.TabIndex = 26;
            // 
            // textBoxPlazoCredito
            // 
            this.textBoxPlazoCredito.Location = new System.Drawing.Point(726, 261);
            this.textBoxPlazoCredito.Name = "textBoxPlazoCredito";
            this.textBoxPlazoCredito.Size = new System.Drawing.Size(240, 20);
            this.textBoxPlazoCredito.TabIndex = 25;
            // 
            // textBoxDescuentoCliente
            // 
            this.textBoxDescuentoCliente.Location = new System.Drawing.Point(726, 209);
            this.textBoxDescuentoCliente.Name = "textBoxDescuentoCliente";
            this.textBoxDescuentoCliente.Size = new System.Drawing.Size(240, 20);
            this.textBoxDescuentoCliente.TabIndex = 24;
            // 
            // textBoxLimiteCredito
            // 
            this.textBoxLimiteCredito.Location = new System.Drawing.Point(726, 157);
            this.textBoxLimiteCredito.Name = "textBoxLimiteCredito";
            this.textBoxLimiteCredito.Size = new System.Drawing.Size(240, 20);
            this.textBoxLimiteCredito.TabIndex = 23;
            // 
            // textBoxCedulaJuridica
            // 
            this.textBoxCedulaJuridica.Location = new System.Drawing.Point(726, 105);
            this.textBoxCedulaJuridica.Name = "textBoxCedulaJuridica";
            this.textBoxCedulaJuridica.Size = new System.Drawing.Size(240, 20);
            this.textBoxCedulaJuridica.TabIndex = 22;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(726, 53);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(240, 20);
            this.textBoxEmail.TabIndex = 21;
            // 
            // textBoxContacto
            // 
            this.textBoxContacto.Location = new System.Drawing.Point(251, 365);
            this.textBoxContacto.Name = "textBoxContacto";
            this.textBoxContacto.Size = new System.Drawing.Size(240, 20);
            this.textBoxContacto.TabIndex = 20;
            // 
            // textBoxFax
            // 
            this.textBoxFax.Location = new System.Drawing.Point(251, 313);
            this.textBoxFax.Name = "textBoxFax";
            this.textBoxFax.Size = new System.Drawing.Size(240, 20);
            this.textBoxFax.TabIndex = 19;
            // 
            // textBoxCelular
            // 
            this.textBoxCelular.Location = new System.Drawing.Point(251, 261);
            this.textBoxCelular.Name = "textBoxCelular";
            this.textBoxCelular.Size = new System.Drawing.Size(240, 20);
            this.textBoxCelular.TabIndex = 18;
            // 
            // textBoxTelefono
            // 
            this.textBoxTelefono.Location = new System.Drawing.Point(251, 209);
            this.textBoxTelefono.Name = "textBoxTelefono";
            this.textBoxTelefono.Size = new System.Drawing.Size(240, 20);
            this.textBoxTelefono.TabIndex = 17;
            // 
            // textBoxDireccion
            // 
            this.textBoxDireccion.Location = new System.Drawing.Point(251, 157);
            this.textBoxDireccion.Name = "textBoxDireccion";
            this.textBoxDireccion.Size = new System.Drawing.Size(240, 20);
            this.textBoxDireccion.TabIndex = 16;
            // 
            // textBoxCodigoCliente
            // 
            this.textBoxCodigoCliente.Location = new System.Drawing.Point(251, 105);
            this.textBoxCodigoCliente.Name = "textBoxCodigoCliente";
            this.textBoxCodigoCliente.Size = new System.Drawing.Size(240, 20);
            this.textBoxCodigoCliente.TabIndex = 15;
            // 
            // textBoxNombreCliente
            // 
            this.textBoxNombreCliente.Location = new System.Drawing.Point(251, 53);
            this.textBoxNombreCliente.Name = "textBoxNombreCliente";
            this.textBoxNombreCliente.Size = new System.Drawing.Size(240, 20);
            this.textBoxNombreCliente.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(560, 365);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(112, 20);
            this.label28.TabIndex = 13;
            this.label28.Text = "Cliente Exento";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(560, 313);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 20);
            this.label27.TabIndex = 12;
            this.label27.Text = "Estatus";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(560, 261);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(125, 20);
            this.label26.TabIndex = 11;
            this.label26.Text = "Plazo de Crédito";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(560, 209);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 20);
            this.label25.TabIndex = 10;
            this.label25.Text = "Descuento";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(560, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(128, 20);
            this.label24.TabIndex = 9;
            this.label24.Text = "Límite de Crédito";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(560, 105);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(117, 20);
            this.label23.TabIndex = 8;
            this.label23.Text = "Cédula Jurídica";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(560, 53);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 20);
            this.label22.TabIndex = 7;
            this.label22.Text = "Email";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(59, 365);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 20);
            this.label21.TabIndex = 6;
            this.label21.Text = "Contacto";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(59, 261);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 20);
            this.label20.TabIndex = 5;
            this.label20.Text = "Celular";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(59, 313);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 20);
            this.label19.TabIndex = 4;
            this.label19.Text = "Fax";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(59, 209);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 20);
            this.label18.TabIndex = 3;
            this.label18.Text = "Teléfono";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(59, 157);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Dirección";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(59, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 20);
            this.label16.TabIndex = 1;
            this.label16.Text = "Código del Cliente";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(59, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(143, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Nombre del Cliente";
            // 
            // panelClientes
            // 
            this.panelClientes.Controls.Add(this.panel5);
            this.panelClientes.Controls.Add(this.dataGridViewClientes);
            this.panelClientes.Location = new System.Drawing.Point(200, 68);
            this.panelClientes.Name = "panelClientes";
            this.panelClientes.Size = new System.Drawing.Size(1107, 641);
            this.panelClientes.TabIndex = 28;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.buttonNuevoCliente);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1107, 62);
            this.panel5.TabIndex = 2;
            // 
            // buttonNuevoCliente
            // 
            this.buttonNuevoCliente.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonNuevoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNuevoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNuevoCliente.ForeColor = System.Drawing.Color.White;
            this.buttonNuevoCliente.Location = new System.Drawing.Point(959, 17);
            this.buttonNuevoCliente.Name = "buttonNuevoCliente";
            this.buttonNuevoCliente.Size = new System.Drawing.Size(123, 26);
            this.buttonNuevoCliente.TabIndex = 0;
            this.buttonNuevoCliente.Text = "Nuevo Cliente";
            this.buttonNuevoCliente.UseVisualStyleBackColor = false;
            this.buttonNuevoCliente.Click += new System.EventHandler(this.buttonNuevoCliente_Click);
            // 
            // dataGridViewClientes
            // 
            this.dataGridViewClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewClientes.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewClientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewClientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.contacto,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn7});
            this.dataGridViewClientes.EnableHeadersVisualStyles = false;
            this.dataGridViewClientes.GridColor = System.Drawing.Color.White;
            this.dataGridViewClientes.Location = new System.Drawing.Point(21, 123);
            this.dataGridViewClientes.Name = "dataGridViewClientes";
            this.dataGridViewClientes.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewClientes.RowHeadersVisible = false;
            this.dataGridViewClientes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewClientes.Size = new System.Drawing.Size(1061, 495);
            this.dataGridViewClientes.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo Cliente";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Teléfono";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Celular";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // contacto
            // 
            this.contacto.HeaderText = "Contacto";
            this.contacto.Name = "contacto";
            this.contacto.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Email";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Estado";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // panelNuevoProveedor
            // 
            this.panelNuevoProveedor.Controls.Add(this.panel7);
            this.panelNuevoProveedor.Location = new System.Drawing.Point(200, 68);
            this.panelNuevoProveedor.Name = "panelNuevoProveedor";
            this.panelNuevoProveedor.Size = new System.Drawing.Size(1107, 641);
            this.panelNuevoProveedor.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.botonGuardarProveedor);
            this.panel7.Controls.Add(this.textBoxPlazoCreditoProveedor);
            this.panel7.Controls.Add(this.textBoxEmailProveedor);
            this.panel7.Controls.Add(this.textBoxContactoProveedor);
            this.panel7.Controls.Add(this.textBoxFaxProveedor);
            this.panel7.Controls.Add(this.textBoxCelularProveedor);
            this.panel7.Controls.Add(this.textBoxTelefonoProveedor);
            this.panel7.Controls.Add(this.textBoxDireccionProveedor);
            this.panel7.Controls.Add(this.textBoxCodigoProveedor);
            this.panel7.Controls.Add(this.textBoxNombreProveedor);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.label36);
            this.panel7.Controls.Add(this.label37);
            this.panel7.Controls.Add(this.label38);
            this.panel7.Controls.Add(this.label39);
            this.panel7.Controls.Add(this.label40);
            this.panel7.Controls.Add(this.label41);
            this.panel7.Controls.Add(this.label42);
            this.panel7.Location = new System.Drawing.Point(42, 29);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1023, 583);
            this.panel7.TabIndex = 1;
            // 
            // botonGuardarProveedor
            // 
            this.botonGuardarProveedor.Location = new System.Drawing.Point(866, 512);
            this.botonGuardarProveedor.Name = "botonGuardarProveedor";
            this.botonGuardarProveedor.Size = new System.Drawing.Size(75, 23);
            this.botonGuardarProveedor.TabIndex = 28;
            this.botonGuardarProveedor.Text = "Guardar";
            this.botonGuardarProveedor.UseVisualStyleBackColor = true;
            this.botonGuardarProveedor.Click += new System.EventHandler(this.botonGuardarProveedor_Click);
            // 
            // textBoxPlazoCreditoProveedor
            // 
            this.textBoxPlazoCreditoProveedor.Location = new System.Drawing.Point(485, 470);
            this.textBoxPlazoCreditoProveedor.Name = "textBoxPlazoCreditoProveedor";
            this.textBoxPlazoCreditoProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxPlazoCreditoProveedor.TabIndex = 25;
            // 
            // textBoxEmailProveedor
            // 
            this.textBoxEmailProveedor.Location = new System.Drawing.Point(485, 418);
            this.textBoxEmailProveedor.Name = "textBoxEmailProveedor";
            this.textBoxEmailProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxEmailProveedor.TabIndex = 21;
            // 
            // textBoxContactoProveedor
            // 
            this.textBoxContactoProveedor.Location = new System.Drawing.Point(485, 366);
            this.textBoxContactoProveedor.Name = "textBoxContactoProveedor";
            this.textBoxContactoProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxContactoProveedor.TabIndex = 20;
            // 
            // textBoxFaxProveedor
            // 
            this.textBoxFaxProveedor.Location = new System.Drawing.Point(485, 314);
            this.textBoxFaxProveedor.Name = "textBoxFaxProveedor";
            this.textBoxFaxProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxFaxProveedor.TabIndex = 19;
            // 
            // textBoxCelularProveedor
            // 
            this.textBoxCelularProveedor.Location = new System.Drawing.Point(485, 262);
            this.textBoxCelularProveedor.Name = "textBoxCelularProveedor";
            this.textBoxCelularProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxCelularProveedor.TabIndex = 18;
            // 
            // textBoxTelefonoProveedor
            // 
            this.textBoxTelefonoProveedor.Location = new System.Drawing.Point(485, 210);
            this.textBoxTelefonoProveedor.Name = "textBoxTelefonoProveedor";
            this.textBoxTelefonoProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxTelefonoProveedor.TabIndex = 17;
            // 
            // textBoxDireccionProveedor
            // 
            this.textBoxDireccionProveedor.Location = new System.Drawing.Point(485, 158);
            this.textBoxDireccionProveedor.Name = "textBoxDireccionProveedor";
            this.textBoxDireccionProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxDireccionProveedor.TabIndex = 16;
            // 
            // textBoxCodigoProveedor
            // 
            this.textBoxCodigoProveedor.Location = new System.Drawing.Point(485, 106);
            this.textBoxCodigoProveedor.Name = "textBoxCodigoProveedor";
            this.textBoxCodigoProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxCodigoProveedor.TabIndex = 15;
            // 
            // textBoxNombreProveedor
            // 
            this.textBoxNombreProveedor.Location = new System.Drawing.Point(485, 54);
            this.textBoxNombreProveedor.Name = "textBoxNombreProveedor";
            this.textBoxNombreProveedor.Size = new System.Drawing.Size(240, 20);
            this.textBoxNombreProveedor.TabIndex = 14;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(293, 470);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(125, 20);
            this.label31.TabIndex = 11;
            this.label31.Text = "Plazo de Crédito";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(293, 418);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(48, 20);
            this.label35.TabIndex = 7;
            this.label35.Text = "Email";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(293, 366);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(74, 20);
            this.label36.TabIndex = 6;
            this.label36.Text = "Contacto";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(293, 262);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 5;
            this.label37.Text = "Celular";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(293, 314);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(35, 20);
            this.label38.TabIndex = 4;
            this.label38.Text = "Fax";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(293, 210);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(71, 20);
            this.label39.TabIndex = 3;
            this.label39.Text = "Teléfono";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(293, 158);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 20);
            this.label40.TabIndex = 2;
            this.label40.Text = "Dirección";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(293, 106);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(160, 20);
            this.label41.TabIndex = 1;
            this.label41.Text = "Código del Proveedor";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(293, 54);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(166, 20);
            this.label42.TabIndex = 0;
            this.label42.Text = "Nombre del Proveedor";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(731, 474);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "Días";
            // 
            // textBoxExistencia
            // 
            this.textBoxExistencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxExistencia.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxExistencia.Location = new System.Drawing.Point(380, 307);
            this.textBoxExistencia.Name = "textBoxExistencia";
            this.textBoxExistencia.Size = new System.Drawing.Size(500, 25);
            this.textBoxExistencia.TabIndex = 17;
            // 
            // checkBoxExcento
            // 
            this.checkBoxExcento.AutoSize = true;
            this.checkBoxExcento.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxExcento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxExcento.Location = new System.Drawing.Point(292, 455);
            this.checkBoxExcento.Name = "checkBoxExcento";
            this.checkBoxExcento.Size = new System.Drawing.Size(78, 24);
            this.checkBoxExcento.TabIndex = 18;
            this.checkBoxExcento.Text = "Exento";
            this.checkBoxExcento.UseVisualStyleBackColor = true;
            // 
            // VentanaMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1307, 709);
            this.Controls.Add(this.panelProveedores);
            this.Controls.Add(this.panelFacturacion);
            this.Controls.Add(this.panelInventario);
            this.Controls.Add(this.panelNuevoCliente);
            this.Controls.Add(this.panelNuevoProveedor);
            this.Controls.Add(this.panelNuevoProducto);
            this.Controls.Add(this.panelClientes);
            this.Controls.Add(this.panelMesas);
            this.Controls.Add(this.panelIzq);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "VentanaMenu";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "VentanaMenu";
            this.panelIzq.ResumeLayout(false);
            this.panelSuperiorInventario.ResumeLayout(false);
            this.panelSuperiorInventario.PerformLayout();
            this.panelInventario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInventario)).EndInit();
            this.panelNuevoProducto.ResumeLayout(false);
            this.panelNuevoProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMesas.ResumeLayout(false);
            this.panelFacturacion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFactura)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelProveedores.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProveedores)).EndInit();
            this.panelNuevoCliente.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelClientes.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientes)).EndInit();
            this.panelNuevoProveedor.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelIzq;
        private System.Windows.Forms.Button buttonProveedores;
        private System.Windows.Forms.Button buttonAjustes;
        private System.Windows.Forms.Button buttonMesas;
        private System.Windows.Forms.Button buttonProductos;
        private System.Windows.Forms.Button buttonInicio;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelSuperiorInventario;
        private System.Windows.Forms.Button botonBuscarProducto;
        private System.Windows.Forms.TextBox textBoxProducto;
        private System.Windows.Forms.Panel panelInventario;
        private System.Windows.Forms.DataGridView dataGridViewInventario;
        private System.Windows.Forms.Panel panelNuevoProducto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPrecio;
        private System.Windows.Forms.TextBox textBoxCosto;
        private System.Windows.Forms.TextBox textBoxProveedor;
        private System.Windows.Forms.TextBox textBoxDescripcion;
        private System.Windows.Forms.TextBox textBoxCodigoProducto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buttonFacturacion;
        private System.Windows.Forms.Button buttonClientes;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo_articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn exitencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn exento;
        private System.Windows.Forms.DataGridViewTextBoxColumn costo;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio;
        private System.Windows.Forms.Panel panelMesas;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Panel panelFacturacion;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewFactura;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button botonAgregar;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBoxFactura;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Button buttonMenos;
        private System.Windows.Forms.Button buttonMas;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxImpuesto;
        private System.Windows.Forms.TextBox textBoxDescuento;
        private System.Windows.Forms.TextBox textBoxSubTotalGravado;
        private System.Windows.Forms.TextBox textBoxSubTotalExcento;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxImpuestoServicio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripción;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio_factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.Panel panelProveedores;
        private System.Windows.Forms.Panel panelNuevoCliente;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxContacto;
        private System.Windows.Forms.TextBox textBoxFax;
        private System.Windows.Forms.TextBox textBoxCelular;
        private System.Windows.Forms.TextBox textBoxTelefono;
        private System.Windows.Forms.TextBox textBoxDireccion;
        private System.Windows.Forms.TextBox textBoxCodigoCliente;
        private System.Windows.Forms.TextBox textBoxNombreCliente;
        private System.Windows.Forms.TextBox textBoxPlazoCredito;
        private System.Windows.Forms.TextBox textBoxDescuentoCliente;
        private System.Windows.Forms.TextBox textBoxLimiteCredito;
        private System.Windows.Forms.TextBox textBoxCedulaJuridica;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.CheckBox checkBoxExento;
        private System.Windows.Forms.ComboBox comboBoxEstatus;
        private System.Windows.Forms.Panel panelClientes;
        private System.Windows.Forms.DataGridView dataGridViewClientes;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonNuevoCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn contacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.DataGridView dataGridViewProveedores;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonNuevoProveedor;
        private System.Windows.Forms.Panel panelNuevoProveedor;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button botonGuardarProveedor;
        private System.Windows.Forms.TextBox textBoxPlazoCreditoProveedor;
        private System.Windows.Forms.TextBox textBoxEmailProveedor;
        private System.Windows.Forms.TextBox textBoxContactoProveedor;
        private System.Windows.Forms.TextBox textBoxFaxProveedor;
        private System.Windows.Forms.TextBox textBoxCelularProveedor;
        private System.Windows.Forms.TextBox textBoxTelefonoProveedor;
        private System.Windows.Forms.TextBox textBoxDireccionProveedor;
        private System.Windows.Forms.TextBox textBoxCodigoProveedor;
        private System.Windows.Forms.TextBox textBoxNombreProveedor;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBoxExistencia;
        private System.Windows.Forms.CheckBox checkBoxExcento;
    }
}