﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;
using AplicacionPizza.Clases;

namespace AplicacionPizza
{
    public partial class VentanaMenu : Form
    {
        public int x = 0;
        public int y = 0;

        public VentanaMenu()
        {
            InitializeComponent();         
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            this.Hide();
            VentanaInventario inventario = new VentanaInventario();
            inventario.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            OcultarPaneles();
            Conexion conectar = new Conexion();

            AutoCompleteStringCollection fuente = new AutoCompleteStringCollection();
            string query = "SELECT * FROM inventario";

            if (conectar.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                dataGridViewInventario.Rows.Clear();
                dataGridViewInventario.Refresh();
                while (dataReader.Read())
                {
                    fuente.Add(dataReader.GetString(0));
                    fuente.Add(dataReader.GetString(1));
                    fuente.Add(dataReader.GetString(2));
                    string[] row = new string[] { dataReader.GetString(0), dataReader.GetString(1), dataReader.GetString(2), dataReader.GetString(3),
                        dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(6)};
                    dataGridViewInventario.Rows.Add(row);
                }
            }
            textBoxProducto.AutoCompleteCustomSource = fuente;
            panelInventario.Show();
            panelSuperiorInventario.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelNuevoProducto.Show();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (textBoxCodigoProducto.Text.Equals(""))
            {
                MessageBox.Show("Código de Producto Vacío");
            }
            else {

                try
                {
                    
                }
                catch (Exception E) {
                    MessageBox.Show("ERROR en el Costo");

                }

                try
                {
                    Producto producto = new Producto(textBoxCodigoProducto.Text, textBoxDescripcion.Text, textBoxProveedor.Text, textBoxExistencia.Text,
                        checkBoxExcento.Checked.ToString(), Convert.ToDouble(textBoxCosto.Text), Convert.ToDouble(textBoxPrecio.Text));
                    
                    Conexion conectar = new Conexion();
                    conectar.InsertarProductos(producto);

                }
                catch (Exception E)
                {
                    MessageBox.Show("ERROR en el Precio");
                }
            }
        }

        private void dataGridViewInventario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            panelNuevoProducto.BringToFront();
            int x = dataGridViewInventario.CurrentCellAddress.X;


        }

        private void button11_Click(object sender, EventArgs e)
        {
            string nombre = Interaction.InputBox("Nombre de la Mesa");
            
            Button BTNtemp = new Button();
            BTNtemp.Left = x;
            BTNtemp.Top = y;
            BTNtemp.Size = new Size(100, 100);
            BTNtemp.Name = nombre;
            BTNtemp.Text = "holiss";
            BTNtemp.Parent = panelMesas;
            BTNtemp.Show();
            BTNtemp.Click += new EventHandler((sender1, e1) => MetodoClicDelBoton(sender1, e1, BTNtemp));
            x+=200;
            y+=200;
        }


        private void MetodoClicDelBoton(object sender, EventArgs e, Button nombre)
        {
            button11.Text = nombre.Name;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelMesas.Show();
        }

        private void OcultarPaneles() {

            panelFacturacion.Hide();
            panelMesas.Hide();
            panelInventario.Hide();
            panelSuperiorInventario.Hide();
            panelNuevoProducto.Hide();
            panelNuevoCliente.Hide();
            panelProveedores.Hide();
            panelClientes.Hide();
            panelProveedores.Hide();
            panelNuevoProveedor.Hide();
       }

        private void button8_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelFacturacion.Show();
            AutoCompleteStringCollection fuente = new AutoCompleteStringCollection();

            Conexion conectar = new Conexion();

            string query = "SELECT * FROM inventario";

            if (conectar.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    fuente.Add(dataReader.GetString(0));
                    fuente.Add(dataReader.GetString(1));
                }
            }
            textBoxFactura.AutoCompleteCustomSource = fuente;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            string buscar = textBoxFactura.Text;
            if (textBoxFactura.Text != "") { 
           
                Conexion conectar = new Conexion();

                string query = "SELECT * FROM inventario";

                if (conectar.OpenConnection())
                {
                    MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        if (buscar == dataReader.GetString(0) || buscar == dataReader.GetString(1)) {
                            string[] row = new string[] { dataReader.GetString(0), dataReader.GetString(1), dataReader.GetString(6),"0", "1",
                                dataReader.GetString(6) };
                            dataGridViewFactura.Rows.Add(row);
                        }
                    }
                }

                textBoxFactura.Text = "";
                ActualizarValores();
                }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelNuevoProducto.Show();
        }

        private void button24_Click(object sender, EventArgs e)
        {
            try
            {
                int actual = dataGridViewFactura.CurrentCell.RowIndex;
                int cantidad = Convert.ToInt16(dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value.ToString());
                cantidad--;
                if (cantidad > 0)
                {
                    double precio = Convert.ToDouble(dataGridViewFactura.Rows[actual].Cells[2].Value.ToString());
                    dataGridViewFactura.Rows[actual].Cells["Total"].Value = Convert.ToString(precio * cantidad);
                    dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value = Convert.ToString(cantidad);
                    dataGridViewFactura.Refresh();
                }
                ActualizarValores();
            }
            catch (Exception E) { }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            try
            {
                int actual = dataGridViewFactura.CurrentCell.RowIndex;
                dataGridViewFactura.Rows.RemoveAt(actual);
                ActualizarValores();
            }
            catch (Exception E) { }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                int actual = dataGridViewFactura.CurrentCell.RowIndex;
                int cantidad = Convert.ToInt16(dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value.ToString());
                cantidad++;
                double precio = Convert.ToDouble(dataGridViewFactura.Rows[actual].Cells[2].Value.ToString());
                dataGridViewFactura.Rows[actual].Cells["Total"].Value = Convert.ToString(precio * cantidad);
                dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value = Convert.ToString(cantidad);
                dataGridViewFactura.Refresh();
                ActualizarValores();
            }
            catch(Exception E) { }
        }

        private void ActualizarValores() {
            int cantidad = dataGridViewFactura.Rows.Count;
            Double subtotal = 0.0;

            foreach (DataGridViewRow row in dataGridViewFactura.Rows)
                subtotal += Convert.ToDouble(row.Cells["Total"].Value);

            textBoxSubTotalExcento.Text = "₡ " + subtotal;
            textBoxImpuesto.Text = "₡ " + (subtotal*0.13);
            textBoxTotal.Text = "₡ " + ((subtotal * 0.13) + subtotal);


        }

        private void button7_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelProveedores.Show();

            Conexion conectar = new Conexion();

            string query = "SELECT * FROM proveedores";

            if (conectar.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                dataGridViewProveedores.Rows.Clear();

                while (dataReader.Read())
                {
                    string[] row = new string[] { dataReader.GetString(0), dataReader.GetString(1), dataReader.GetString(2), dataReader.GetString(3),
                        dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(6), dataReader.GetString(7), dataReader.GetString(8) };
                    dataGridViewProveedores.Rows.Add(row);
                }
                dataGridViewProveedores.Refresh();

            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelClientes.Show();

         
            Conexion conectar = new Conexion();

            string query = "SELECT * FROM clientes";

            if (conectar.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                dataGridViewClientes.Rows.Clear();
                dataGridViewClientes.Refresh();

                while (dataReader.Read())
                {
                    string[] row = new string[] { dataReader.GetString(0), dataReader.GetString(1), dataReader.GetString(3), dataReader.GetString(5),
                        dataReader.GetString(6), dataReader.GetString(7), dataReader.GetString(11) };
                    dataGridViewClientes.Rows.Add(row);
                }
  
            }

        }

        private void buttonNuevoCliente_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelNuevoCliente.Show();

        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {           
            try
            {
                Cliente cliente = new Cliente(textBoxCodigoCliente.Text, textBoxNombreCliente.Text, textBoxDireccion.Text, textBoxTelefono.Text, textBoxFax.Text,
                    textBoxCelular.Text, textBoxContacto.Text, textBoxEmail.Text, textBoxCedulaJuridica.Text, textBoxLimiteCredito.Text, textBoxDescuento.Text,
                    textBoxPlazoCredito.Text, comboBoxEstatus.Text.ToString(), checkBoxExento.Checked.ToString());
               
                Conexion conectar = new Conexion();
                conectar.InsertarClientes(cliente);
            }
            catch (Exception E)
            {
                MessageBox.Show("ERROR en el Ingreso de Datos "+ E);
            }
            
        }

        private void dataGridViewFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewFactura.Rows.Count != 0) {
                int fila = dataGridViewFactura.CurrentCell.RowIndex;
                int columna = dataGridViewFactura.CurrentCell.ColumnIndex;

                if (columna == 3)
                {
                    //MessageBox.Show("Datos cambiados");
                    int actual = dataGridViewFactura.CurrentCell.RowIndex;
                    int cantidad = Convert.ToInt16(dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value.ToString());
                    double descuento = Convert.ToInt16(dataGridViewFactura.Rows[actual].Cells["Descuento"].Value.ToString());
                    double precio = Convert.ToDouble(dataGridViewFactura.Rows[actual].Cells[2].Value.ToString());
                    try
                    {
                        


                        dataGridViewFactura.Rows[actual].Cells["Total"].Value = Convert.ToString((precio - (descuento / 100 * precio)) * cantidad);
                        dataGridViewFactura.Rows[actual].Cells["Cantidad"].Value = Convert.ToString(cantidad);
                        dataGridViewFactura.Rows[actual].Cells[2].Value = Convert.ToString((precio - (descuento / 100 * precio)));
                        dataGridViewFactura.Refresh();
                        ActualizarValores();
                    }
                    catch (Exception E) { }
                }
            }
        }

        private void textBoxFactura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                botonAgregar.PerformClick();
            }
        }

        private void buttonNuevoProveedor_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            panelNuevoProveedor.Show();
        }

        private void botonGuardarProveedor_Click(object sender, EventArgs e)
        {
            Proveedor proveedor = new Proveedor(textBoxCodigoProveedor.Text, textBoxNombreProveedor.Text, textBoxDireccionProveedor.Text,
                textBoxTelefonoProveedor.Text,textBoxFaxProveedor.Text, textBoxCelularProveedor.Text, textBoxContactoProveedor.Text, 
                textBoxEmailProveedor.Text, textBoxPlazoCreditoProveedor.Text);
            
            Conexion conectar = new Conexion();
            conectar.InsertarProveedores(proveedor);
        }

        private void botonBuscarProducto_Click(object sender, EventArgs e)
        {
            if (!textBoxProducto.Equals("")) {

                Conexion conectar = new Conexion();
                string query = "SELECT * FROM inventario";

                if (conectar.OpenConnection())
                {
                    MySqlCommand cmd = new MySqlCommand(query, conectar.connection);
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    dataGridViewInventario.Rows.Clear();
                    dataGridViewInventario.Refresh();
                    while (dataReader.Read())
                    {
                        if (textBoxProducto.Text.Equals(dataReader.GetString(0)) || textBoxProducto.Text.Equals(dataReader.GetString(1)) 
                            || textBoxProducto.Text.Equals(dataReader.GetString(2))) { 
                            string[] row = new string[] { dataReader.GetString(0), dataReader.GetString(1), dataReader.GetString(2), dataReader.GetString(3),
                            dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(6)};
                            dataGridViewInventario.Rows.Add(row);
                        }
                    }
                }
            }
        }
    }
}
